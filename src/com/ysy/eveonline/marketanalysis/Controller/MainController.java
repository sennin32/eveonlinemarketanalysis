package com.ysy.eveonline.marketanalysis.Controller;

import com.ysy.eveonline.marketanalysis.Data.*;
import com.ysy.eveonline.marketanalysis.httpevent.events.LookItem;
import com.ysy.eveonline.marketanalysis.httpevent.events.UpdateStationInfo;
import com.ysy.eveonline.marketanalysis.module.DbConnector;
import com.ysy.eveonline.marketanalysis.module.SystemJumpCalculator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainController {
    public static String INFO_BEFORE_GET_DATA = "loading data";
    public static String INFO_AFTER_GET_DATA = "loading data finished";

    public static String FLAG_PUBLISH_PROGRESS = "progress";

    public Label text_status;
    public Button button_calculate;

    public TableView table_market_info;
    public TableColumn ColumnItem, ColumnEarn, ColumnFrom, ColumnTo, ColumnBuyAs, ColumnSellAs,
            ColumnBuyCount, ColumnSellCount, ColumnJumpsToBuy, ColumnJumps;
    ExecutorService gExecutorService = Executors.newFixedThreadPool(5);
    private ObservableList<ItemInMarket> gTableData = FXCollections.observableArrayList();

    private ArrayList<ItemType> ListItemType;
    private ArrayList<SolarSystem> ListSystemType;
    private ArrayList<ItemInMarket> ListItemInMarket;
    private ArrayList<Station> ListStation;

    private int countTaskFinished;
    private static int countTaskHasToBeDone = 3;

    public void getData() {
        countTaskFinished = 0;
        text_status.setText(INFO_BEFORE_GET_DATA);
        ListItemType = new ArrayList<ItemType>();
        ListSystemType = new ArrayList<SolarSystem>();
        ListItemInMarket = new ArrayList<ItemInMarket>();
        ListStation = new ArrayList<Station>();

        refreshStationList();
        refreshItemList();
        refreshSystemList();
    }

    public void updateStationList() {
        UpdateStationInfo mUpdateStationInfo = new UpdateStationInfo();
        new Thread(mUpdateStationInfo).start();

        mUpdateStationInfo.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                countTaskFinished += 1;
                if (countTaskFinished >= countTaskHasToBeDone)
                    loadFinish();
            }
        });
        new Thread(mUpdateStationInfo).start();
    }

    public void refreshStationList() {
        Task TaskGetItemList = new Task<ArrayList<Station>>() {
            @Override protected ArrayList<Station> call(){
                ListStation.addAll(DbConnector.getStationList());
                return ListStation;
            }
        };

        TaskGetItemList.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                countTaskFinished += 1;
                if (countTaskFinished >= countTaskHasToBeDone)
                    loadFinish();
            }
        });
        new Thread(TaskGetItemList).start();
    }

    public void refreshItemList() {
        Task TaskGetItemList = new Task<ArrayList<ItemType>>() {
            @Override protected ArrayList<ItemType> call(){
                ListItemType.addAll(DbConnector.getItemList());
                return ListItemType;
            }
        };

        TaskGetItemList.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                countTaskFinished += 1;
                if (countTaskFinished >= countTaskHasToBeDone)
                    loadFinish();
            }
        });
        new Thread(TaskGetItemList).start();
    }

    public void refreshSystemList() {
        Task TaskGetItemList = new Task<ArrayList<SolarSystem>>() {
            @Override protected ArrayList<SolarSystem> call(){
                ListSystemType.addAll(DbConnector.getSystemList());
                return ListSystemType;
            }
        };

        TaskGetItemList.setOnSucceeded(new EventHandler<WorkerStateEvent>(){
            @Override
            public void handle(WorkerStateEvent t) {
                countTaskFinished += 1;
                if (countTaskFinished >= countTaskHasToBeDone)
                    loadFinish();
            }
        });
        new Thread(TaskGetItemList).start();
    }

    private void loadFinish() {
        button_calculate.setDisable(false);
        text_status.setText(INFO_AFTER_GET_DATA);
        SystemJumpCalculator.getInstance().setSystemList(ListSystemType);
        SystemJumpCalculator.getInstance().setStationList(ListStation);
        for(int i= 0; i<ListStation.size(); i++) {
            for(int j= 0; j<ListSystemType.size(); j++) {
                if(ListStation.get(i).getSystemId()==ListSystemType.get(j).getSystemId()) {
                    ListStation.get(i).setSecurity(ListSystemType.get(j).getSecurity());
                    break;
                }
            }
        }
        setTable();
    }

    public void button_calculate_click(ActionEvent actionEvent) {
        /*Jump mJump = SystemJumpCalculator.getInstance().CalculateStationJumps(60003760, 60011866, 0.4);
        System.out.println("Jumps From  60003760 to 60011866 : "+ mJump.getJumps()+ ", "+ mJump.getLowestSecurity()+
        ", "+mJump.getState());*/
        for(int i = 0; i<ListItemType.size(); i++) {
            LookItem mLookItem = new LookItem(ListItemType.get(i), PreferencesAndSetting.DEF_LOWEST_PERCENTAGE,
                    PreferencesAndSetting.DEF_LOWEST_SECURITY, PreferencesAndSetting.DEF_HIGHEST_JUMP,
                    PreferencesAndSetting.DEF_STATION, PreferencesAndSetting.DEF_LOWEST_ISK,
                    PreferencesAndSetting.DEF_SHIP_VOLUME);
            mLookItem.setOnSucceeded(new EventHandler<WorkerStateEvent>(){
                @Override
                public void handle(WorkerStateEvent t) {
                    ArrayList<ItemInMarket> tMarketList = new ArrayList<ItemInMarket>();
                    tMarketList.addAll((ArrayList<ItemInMarket>) t.getSource().getValue());
                    gTableData.addAll(tMarketList);
                    System.out.println("finish with "+ ListItemInMarket.size() +" recommend!");
                }
            });
            gExecutorService.submit(mLookItem);
        }
    }

    public void setTable(){
        table_market_info.setEditable(false);
        ColumnItem.setCellValueFactory(new PropertyValueFactory<ItemInMarket, String>("ItemName"));
        ColumnEarn.setCellValueFactory(new PropertyValueFactory<ItemInMarket, Double>("PriceDif"));
        ColumnFrom.setCellValueFactory(new PropertyValueFactory<ItemInMarket, String>("StationStartName"));
        ColumnTo.setCellValueFactory(new PropertyValueFactory<ItemInMarket, String>("StationEndName"));
        ColumnBuyAs.setCellValueFactory(new PropertyValueFactory<ItemInMarket, Double>("StartPrice"));
        ColumnSellAs.setCellValueFactory(new PropertyValueFactory<ItemInMarket, Double>("EndPrice"));
        ColumnBuyCount.setCellValueFactory(new PropertyValueFactory<ItemInMarket, Integer>("StartCount"));
        ColumnSellCount.setCellValueFactory(new PropertyValueFactory<ItemInMarket, Integer>("EndCount"));
        ColumnJumpsToBuy.setCellValueFactory(new PropertyValueFactory<ItemInMarket, Integer>("JumpsFromNow"));
        ColumnJumps.setCellValueFactory(new PropertyValueFactory<ItemInMarket, Integer>("Jumps"));
        table_market_info.setItems(gTableData);
    }
}
