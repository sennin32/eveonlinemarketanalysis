package com.ysy.eveonline.marketanalysis.Data;


public enum EnumSellOrBuy {
    FLAG_SELL{
        @Override
        public String getTagName() {
            return "sell_orders";
        }
    },FLAG_BUY{
        @Override
        public String getTagName() {
            return "buy_orders";
        }
    };

    public abstract String getTagName();
}
