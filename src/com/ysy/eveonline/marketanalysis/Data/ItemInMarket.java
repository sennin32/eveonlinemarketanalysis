package com.ysy.eveonline.marketanalysis.Data;

public class ItemInMarket extends ItemType {
    public static int FLAG_UNKNOWN_ERROR = 0;
    public static int FLAG_SUCCESS = 1;
    public static final int FLAG_IS_RECOMMEND = 2;
    public static final int FLAG_NO_DIF = 95;
    public static final int FLAG_TO_FAR = 96;
    public static final int FLAG_NOT_SECURITY = 97;
    public static final int FLAG_SYSTEM_NOT_FOUND = 98;

    private int StationStartID;
    private int StationEndID;
    private String StationStartName;
    private String StationEndName;
    private int Jumps;
    private int JumpsFromNow;
    private double LowestSecurity;
    private double StartPrice;
    private double EndPrice;
    private double PriceDif;
    private double PriceDifPercent;
    private int StartCount;
    private int EndCount;
    private int MinStartCount;
    private int MinEndCount;
    private int State;

    public void setStationStartID(int argId) {
        StationStartID = argId;
    }

    public int getStationStartID() {
        return StationStartID;
    }

    public void setStationEndID(int argId) {
        StationEndID = argId;
    }

    public int getStationEndID() {
        return StationEndID;
    }

    public void setStationStartName(String argId) {
        StationStartName = argId;
    }

    public String getStationStartName() {
        return StationStartName;
    }

    public void setStationEndName(String argId) {
        StationEndName = argId;
    }

    public String getStationEndName() {
        return StationEndName;
    }

    public void setLowestSecurity(double argSecurity) {
        LowestSecurity = argSecurity;
    }

    public double getLowestSecurity() {
        return LowestSecurity;
    }

    public void setEndPrice(double argPrice) {
        EndPrice = argPrice;
    }

    public double getEndPrice() {
        return EndPrice;
    }

    public void setPriceDif(double argPrice) {
        PriceDif = argPrice;
    }

    public double getPriceDif() {
        return PriceDif;
    }

    public void setStartPrice(double argPrice) {
        StartPrice = argPrice;
    }

    public double getStartPrice() {
        return StartPrice;
    }

    public void setPriceDifPercent(double argPriceDifPercent) {
        PriceDifPercent = argPriceDifPercent;
    }

    public double getPriceDifPercent() {
        return PriceDifPercent;
    }

    public void setStartCount(int argVolume) {
        StartCount = argVolume;
    }

    public int getStartCount() {
        return StartCount;
    }

    public void setEndCount(int argVolume) {
        EndCount = argVolume;
    }

    public int getEndCount() {
        return EndCount;
    }

    public void setMinStartCount(int argVolume) {
        MinStartCount = argVolume;
    }

    public int getMinStartCount() {
        return MinStartCount;
    }

    public void setMinEndCount(int argVolume) {
        MinEndCount = argVolume;
    }

    public int getMinEndCount() {
        return MinEndCount;
    }

    public void setJumps(int argJumps) {
        Jumps = argJumps;
    }

    public int getJumps() {
        return Jumps;
    }

    public void setJumpsFromNow(int argJumps) {
        JumpsFromNow = argJumps;
    }

    public int getJumpsFromNow() {
        return JumpsFromNow;
    }

    public void setState(int argId) {
        State = argId;
    }

    public int getState() {
        return State;
    }
}
