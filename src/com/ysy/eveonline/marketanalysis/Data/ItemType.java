package com.ysy.eveonline.marketanalysis.Data;

public class ItemType {
    private String ItemId;
    private String ItemName;
    private double Volume;

    public void setItemId(String argId) {
        ItemId = argId;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemName(String argName) {
        ItemName = argName;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setVolume(double argVolume) {
        Volume = argVolume;
    }

    public double getVolume() {
        return Volume;
    }
}
