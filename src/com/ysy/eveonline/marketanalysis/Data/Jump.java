package com.ysy.eveonline.marketanalysis.Data;

public class Jump {
    public static final int FLAG_SYSTEM_NOT_FOUND = 98;
    public static int FLAG_UNKNOWN_ERROR = 0;
    public static int FLAG_SUCCESS = 1;

    private int Jumps;
    private double LowestSecurity;
    private int State;

    public void setJumps(int argId) {
        Jumps = argId;
    }

    public int getJumps() {
        return Jumps;
    }

    public void setLowestSecurity(double argName) {
        LowestSecurity = argName;
    }

    public double getLowestSecurity() {
        return LowestSecurity;
    }

    public void setState(int argId) {
        State = argId;
    }

    public int getState() {
        return State;
    }
}
