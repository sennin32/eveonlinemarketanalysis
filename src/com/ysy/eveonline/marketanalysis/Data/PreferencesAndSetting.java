package com.ysy.eveonline.marketanalysis.Data;

/**
 * Created by Administrator on 2014/2/16.
 */
public class PreferencesAndSetting {
    private static PreferencesAndSetting gPreferencesAndSetting;

    public static double DEF_LOWEST_PERCENTAGE = 0.2,
            DEF_LOWEST_SECURITY = 0.4,
            DEF_SHIP_VOLUME=5000.0;
    public static int DEF_HIGHEST_JUMP = 5,
            DEF_STATION = 60003760,
            DEF_LOWEST_ISK = 2000000;

    private double gLowestDifPercentage, gLowestSecurity;
    private int gHighestJumps, gNowStation, gLowestISK;

    public PreferencesAndSetting getInstance(){
        if(gPreferencesAndSetting==null)
            gPreferencesAndSetting = new PreferencesAndSetting();
        return gPreferencesAndSetting;
    }

    public PreferencesAndSetting() {
        gLowestDifPercentage = -1.0;
        gLowestSecurity = -1.0;
        gHighestJumps = -1;
        gNowStation = 60003466; // Jita IV moon 4
    }
}
