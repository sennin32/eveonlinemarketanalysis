package com.ysy.eveonline.marketanalysis.Data;

public class Region {
    private String RegionId;
    private String RegionName;

    public void setRegionId(String argId) {
        RegionId = argId;
    }

    public String getRegionId() {
        return RegionId;
    }

    public void setRegionName(String argName) {
        RegionName = argName;
    }

    public String getRegionName() {
        return RegionName;
    }
}
