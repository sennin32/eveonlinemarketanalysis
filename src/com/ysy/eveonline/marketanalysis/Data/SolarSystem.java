package com.ysy.eveonline.marketanalysis.Data;

import java.util.ArrayList;

public class SolarSystem implements Cloneable {
    private int SystemId; // for speed
    private String SystemName;
    private String RegionId;
    private Double Security;
    private Double LowestSecurity;
    private int[] NearbyJumps;
    private int Level = -1; // Used only bfs

    public void setSystemId(int argId) {
        SystemId = argId;
    }

    public int getSystemId() {
        return SystemId;
    }

    public void setSystemName(String argName) {
        SystemName = argName;
    }

    public String getSystemName() {
        return SystemName;
    }

    public void setRegionId(String argId) {
        RegionId = argId;
    }

    public String getRegionId() {
        return RegionId;
    }

    public void setSecurity(Double argSecurity) {
        Security = argSecurity;
    }

    public Double getSecurity() {
        return Security;
    }

    public Double getLowestSecurity() {
        return LowestSecurity;
    }

    public void setLowestSecurity(Double argSecurity) {
        LowestSecurity = argSecurity;
    }

    public void setNearbyJumps(int[] argListIds) {
        NearbyJumps = argListIds;
    }

    public int[] getNearbyJumps() {
        if(NearbyJumps!=null)
            return NearbyJumps;
        else
            return new int[0];
    }

    public void setLevel(int argLevel) {
        Level = argLevel;
    }

    public int getLevel() {
        return Level;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            // This should never happen
            throw new InternalError(e.toString());
        }
    }
}
