package com.ysy.eveonline.marketanalysis.Data;

public class Station {
    private int StationId;
    private String StationName;
    private int SystemId; // for speed
    private Double Security;

    public void setSystemId(int argId) {
        SystemId = argId;
    }

    public int getSystemId() {
        return SystemId;
    }

    public void setStationId(int argId) {
        StationId = argId;
    }

    public int getStationId() {
        return StationId;
    }

    public void setStationName(String argId) {
        StationName = argId;
    }

    public String getStationName() {
        return StationName;
    }

    public void setSecurity(Double argSecurity) {
        Security = argSecurity;
    }

    public Double getSecurity() {
        return Security;
    }
}
