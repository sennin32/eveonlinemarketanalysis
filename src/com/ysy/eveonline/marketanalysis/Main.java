package com.ysy.eveonline.marketanalysis;

import com.ysy.eveonline.marketanalysis.Controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    private static int minWindowWidth = 400;
    private static int minWindowHeight = 300;

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view_main.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root));
        primaryStage.setMinWidth(minWindowWidth);
        primaryStage.setMinHeight(minWindowHeight);

        primaryStage.show();

        MainController controller = loader.<MainController>getController();
        controller.getData();
    }


    public static void main(String[] args) {
        launch(args);
        System.out.println("");
    }
}
