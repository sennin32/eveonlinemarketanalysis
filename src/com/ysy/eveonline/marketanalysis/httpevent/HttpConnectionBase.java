package com.ysy.eveonline.marketanalysis.httpevent;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public abstract class HttpConnectionBase {
    public static int gConnectionTimeOut = 10000;
    public static int gSocketTimeOut     = 15000;

    public static final int SUCCESS = 10001;
    public static final int ERROR   = 19998;

    public static final String CONNECTION_ERROR = "Connection Error";

    public abstract void post();

    public String httpGetQPara(String tEndPoint, List<BasicNameValuePair> nameValuePairs) {
        CloseableHttpClient client = HttpClients.createDefault();
        try {
            if(nameValuePairs!=null && nameValuePairs.size()>0) {
                if(!tEndPoint.endsWith("?"))
                    tEndPoint += "?";
                String paramString = URLEncodedUtils.format(nameValuePairs, "utf-8");
                tEndPoint += paramString;
            }
            System.out.println( "URI : "+tEndPoint );
            HttpGet method = new HttpGet(new URI(tEndPoint));
            HttpResponse lHttpResponse = client.execute(method);

            if ((lHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK)&& lHttpResponse != null) {
                String resp_str = EntityUtils.toString(lHttpResponse.getEntity());
                return resp_str;
            } else if(lHttpResponse.getStatusLine().getStatusCode() == 201) {
                return Integer.toString(lHttpResponse.getStatusLine().getStatusCode());
            } else{
                if(lHttpResponse.getStatusLine().getStatusCode()==500) {
                    return CONNECTION_ERROR;
                }
                else {
                    return CONNECTION_ERROR;
                }
            }
        } catch (UnknownHostException e) {
            System.out.println( "UnknownHostException : "+e.getMessage() );
        } catch (SocketTimeoutException e) {
            System.out.println( "SocketTimeoutException : "+e.getMessage() );
        } catch (UnsupportedEncodingException e) {
            System.out.println( "UnsupportedEncodingException : "+e.getMessage() );
        } catch (ClientProtocolException e) {
            System.out.println( "ClientProtocolException : "+e.getMessage() );
        } catch (IOException e) {
            System.out.println( "IOException : "+e.getMessage() );
        } catch (URISyntaxException e) {
            System.out.println( "URISyntaxException : "+e.getMessage() );
        }
        System.out.println( "Don't know why");
        return CONNECTION_ERROR;
    }
}
