package com.ysy.eveonline.marketanalysis.httpevent.events;

import com.ysy.eveonline.marketanalysis.Data.EnumSellOrBuy;
import com.ysy.eveonline.marketanalysis.Data.ItemInMarket;
import com.ysy.eveonline.marketanalysis.Data.ItemType;
import com.ysy.eveonline.marketanalysis.Data.Jump;
import com.ysy.eveonline.marketanalysis.httpevent.EventBase;
import com.ysy.eveonline.marketanalysis.module.QuickSortItemInMarket;
import com.ysy.eveonline.marketanalysis.module.SystemJumpCalculator;
import javafx.concurrent.Task;
import org.apache.http.message.BasicNameValuePair;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.Iterator;

public class LookItem extends Task<ArrayList<ItemInMarket>> {
    public static final String FLAG_TYPE_ID = "typeid";
    public static String FLAG_HOURS = "sethours";
    public static String FLAG_SELL = "item";
    public static String URL = "http://api.eve-central.com/api/quicklook";
    public static int HOURS = 12;

    private ItemType gItem;
    private static Element gRoot;
    ArrayList<ItemInMarket> gListItemRecommend, gListItemSell, gListItemBuy;
    private double gLowestDifPercentage, gLowestSecurity, gShipVolume;
    private int gHighestJumps, gNowStation, gISKs;

    private class EventBaseLookItem extends EventBase {
        public void post() {
            System.out.println( "post" );
            ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(FLAG_TYPE_ID, gItem.getItemId()));
            nameValuePairs.add(new BasicNameValuePair(FLAG_HOURS, String.valueOf(HOURS)));
            String mResponse = httpGetQPara(URL, nameValuePairs);
            DataParser(mResponse);
        }

        public void DataParser(String argData) {
            Document mDocument = null;
            try {
                mDocument = DocumentHelper.parseText(argData);
                gRoot = mDocument.getRootElement();
                Iterator tIteratorSellBuy = ((Element)gRoot.elementIterator().next()).elementIterator();
                while(tIteratorSellBuy.hasNext()){
                    Element ElementSellAndBuy = (Element)tIteratorSellBuy.next();
                    if(ElementSellAndBuy.getName().equals(EnumSellOrBuy.FLAG_SELL.getTagName())) {
                        Iterator ItrSellItems = ElementSellAndBuy.elementIterator();
                        gListItemSell = new ArrayList<ItemInMarket>();
                        gListItemSell = ItemParser(ItrSellItems, gListItemSell, EnumSellOrBuy.FLAG_SELL);
                    } else if(ElementSellAndBuy.getName().equals(EnumSellOrBuy.FLAG_BUY.getTagName())) {
                        Iterator ItrSellItems = ElementSellAndBuy.elementIterator();
                        gListItemBuy = new ArrayList<ItemInMarket>();
                        gListItemBuy = ItemParser(ItrSellItems, gListItemBuy, EnumSellOrBuy.FLAG_BUY);
                    }
                }
            } catch (DocumentException e) {
            }
            for(int CounterSell=0; CounterSell<gListItemSell.size(); CounterSell++){
                ItemInMarket mItemInMarket;
                Jump mJump = SystemJumpCalculator.getInstance().CalculateStationJumps(gNowStation,
                        gListItemSell.get(CounterSell).getStationStartID(), gLowestSecurity);
                if(mJump.getLowestSecurity()>gLowestSecurity) {
                    for(int CounterBuy=0; CounterBuy<gListItemBuy.size(); CounterBuy++){
                        if(gListItemSell.get(CounterSell).getStartCount()!=0&&
                                gListItemBuy.get(CounterBuy).getEndCount()!=0&&
                                gListItemSell.get(CounterBuy).getStartCount()>=gListItemBuy.get(CounterBuy).getMinEndCount()) {
                            mItemInMarket = isRecommend(gListItemSell.get(CounterSell).getStartPrice(),
                                    gListItemBuy.get(CounterBuy).getEndPrice(),
                                    gListItemSell.get(CounterSell).getStationStartID(),
                                    gListItemBuy.get(CounterBuy).getStationEndID());
                            if(mItemInMarket.getState()==ItemInMarket.FLAG_NO_DIF){
                                if(CounterBuy==0)
                                    return;
                                break;
                            } else if(mItemInMarket.getState()==ItemInMarket.FLAG_IS_RECOMMEND) {
                                mItemInMarket.setItemId(gItem.getItemId());
                                mItemInMarket.setStartCount(gListItemSell.get(CounterSell).getStartCount());
                                mItemInMarket.setEndCount(gListItemBuy.get(CounterBuy).getEndCount());
                                mItemInMarket.setStartPrice(gListItemSell.get(CounterSell).getStartPrice());
                                mItemInMarket.setEndPrice(gListItemBuy.get(CounterBuy).getEndPrice());
                                int MinorCount;
                                if(gListItemSell.get(CounterSell).getStartCount()>gListItemBuy.get(CounterBuy).getEndCount())
                                    MinorCount = gListItemBuy.get(CounterBuy).getEndCount();
                                else
                                    MinorCount = gListItemSell.get(CounterSell).getStartCount();
                                if(gISKs<MinorCount*gListItemSell.get(CounterSell).getStartPrice()) {
                                    MinorCount = (int)(gISKs/gListItemSell.get(CounterSell).getStartPrice());
                                }
                                if(gShipVolume<MinorCount*gItem.getVolume()) {
                                    MinorCount = (int)(gShipVolume/gItem.getVolume());
                                }
                                mItemInMarket.setItemName(gItem.getItemName());
                                mItemInMarket.setVolume(gItem.getVolume());
                                mItemInMarket.setPriceDif((gListItemBuy.get(CounterBuy).getEndPrice() - gListItemSell.get(CounterSell).getEndPrice())*MinorCount);
                                /*System.out.println("price dif : "+gListItemBuy.get(CounterBuy).getEndPrice()
                                        +", "+ gListItemBuy.get(CounterBuy).getEndPrice()+
                                        ", " + MinorCount);*/
                                mItemInMarket.setPriceDifPercent((gListItemBuy.get(CounterBuy).getEndPrice() - gListItemSell.get(CounterSell).getStartPrice() /
                                        gListItemSell.get(CounterSell).getStartPrice()));
                                mItemInMarket.setStationStartID(gListItemSell.get(CounterSell).getStationStartID());
                                mItemInMarket.setStationEndID(gListItemBuy.get(CounterBuy).getStationEndID());
                                mItemInMarket.setStationStartName(gListItemSell.get(CounterSell).getStationStartName());
                                mItemInMarket.setStationEndName(gListItemBuy.get(CounterBuy).getStationEndName());
                                mItemInMarket.setJumpsFromNow(mJump.getJumps());
                                if(mJump.getLowestSecurity()<mItemInMarket.getLowestSecurity())
                                    mItemInMarket.setLowestSecurity(mJump.getLowestSecurity());
                                gListItemRecommend.add(mItemInMarket);
                            }/* else if(mItemInMarket.getState()==ItemInMarket.FLAG_TO_FAR) {
                            } else if(mItemInMarket.getState()==ItemInMarket.FLAG_SYSTEM_NOT_FOUND) {
                            } else if(mItemInMarket.getState()==ItemInMarket.FLAG_NOT_SECURITY){
                            } else {
                            }*/
                        }
                    }
                }
            }
        }

        public ArrayList<ItemInMarket> ItemParser(Iterator ItrList, ArrayList<ItemInMarket> Target, EnumSellOrBuy argSellBuyFlag) {
            while(ItrList.hasNext()) {
                Element tElement = (Element)ItrList.next();
                ItemInMarket tItemInMarket = new ItemInMarket();
                switch (argSellBuyFlag) {
                    case FLAG_SELL:
                        tItemInMarket.setStationStartID(Integer.valueOf(tElement.element("station").getData().toString()));
                        tItemInMarket.setStartPrice(Double.valueOf(tElement.element("price").getData().toString()));
                        tItemInMarket.setStartCount(Integer.valueOf(tElement.element("vol_remain").getData().toString()));
                        tItemInMarket.setStationStartName(tElement.element("station_name").getData().toString());
                        tItemInMarket.setMinStartCount(Integer.valueOf(tElement.element("min_volume").getData().toString()));
                        Target.add(tItemInMarket);
                        break;
                    case FLAG_BUY:
                        tItemInMarket.setStationEndID(Integer.valueOf(tElement.element("station").getData().toString()));
                        tItemInMarket.setEndPrice(Double.valueOf(tElement.element("price").getData().toString()));
                        tItemInMarket.setEndCount(Integer.valueOf(tElement.element("vol_remain").getData().toString()));
                        tItemInMarket.setStationEndName(tElement.element("station_name").getData().toString());
                        tItemInMarket.setMinEndCount(Integer.valueOf(tElement.element("min_volume").getData().toString()));
                        Target.add(tItemInMarket);
                        break;
                }
            }
            QuickSortItemInMarket mQuickSortItemInMarket = new QuickSortItemInMarket(Target, argSellBuyFlag);
            return mQuickSortItemInMarket.doSort(0, Target.size()-1);
        }

        private ItemInMarket isRecommend(Double argStartPrice, Double argEndPrice, int StartId, int EndId ) {
            ItemInMarket mItemInMarket = new ItemInMarket();
            if((argEndPrice-argStartPrice)/argStartPrice>gLowestDifPercentage) {
                Jump mJumps = SystemJumpCalculator.getInstance().CalculateStationJumps(StartId, EndId, gLowestSecurity);
                if(mJumps.getState()==Jump.FLAG_SYSTEM_NOT_FOUND) {
                    mItemInMarket.setState(ItemInMarket.FLAG_SYSTEM_NOT_FOUND);
                } else if(mJumps.getLowestSecurity()<gLowestSecurity){
                    mItemInMarket.setState(ItemInMarket.FLAG_NOT_SECURITY);
                } else if(mJumps.getState()==Jump.FLAG_UNKNOWN_ERROR){
                    mItemInMarket.setState(ItemInMarket.FLAG_UNKNOWN_ERROR);
                } else if(mJumps.getJumps() <= gHighestJumps&& mJumps.getJumps()!= -1) {
                    mItemInMarket.setState(ItemInMarket.FLAG_IS_RECOMMEND);
                    mItemInMarket.setJumps(mJumps.getJumps());
                    mItemInMarket.setLowestSecurity(mJumps.getLowestSecurity());
                } else
                    mItemInMarket.setState(ItemInMarket.FLAG_TO_FAR);
            } else {
                mItemInMarket.setState(ItemInMarket.FLAG_NO_DIF);
            }
            return mItemInMarket;
        }
    }

    public LookItem(ItemType argType, double argLowestDifPercentage, double argLowestSecurity, int argHighestJumps,
                    int argNowStation, int argISK, double argShipVolume){
        gItem = argType;
        gLowestDifPercentage = argLowestDifPercentage;
        gHighestJumps = argHighestJumps;
        gLowestSecurity = argLowestSecurity;
        gNowStation = argNowStation;
        gISKs = argISK;
        gShipVolume = argShipVolume;
    }

    @Override protected ArrayList<ItemInMarket> call(){
        gListItemRecommend = new ArrayList<ItemInMarket>();
        EventBaseLookItem mEventBaseLookItem = new EventBaseLookItem();
        mEventBaseLookItem.post();
        return gListItemRecommend;
    }
}
