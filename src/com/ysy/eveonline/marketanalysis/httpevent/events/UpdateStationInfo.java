package com.ysy.eveonline.marketanalysis.httpevent.events;

import com.ysy.eveonline.marketanalysis.Data.Station;
import com.ysy.eveonline.marketanalysis.httpevent.EventBase;
import com.ysy.eveonline.marketanalysis.module.DbConnector;
import javafx.concurrent.Task;
import org.apache.http.message.BasicNameValuePair;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.Iterator;

public class UpdateStationInfo extends Task<ArrayList<Station>> {
    public static String FLAG_RESULT = "result";
    public static String URL = "http://api.eveonline.com/eve/ConquerableStationList.xml.aspx";

    private static Element gRoot;
    ArrayList<Station> gListStation;

    private class EventBaseLookItem extends EventBase {
        public void post() {
            ArrayList<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>();
            String mResponse = httpGetQPara(URL, nameValuePairs);
            DataParser(mResponse);
        }

        public void DataParser(String argData) {
            Document mDocument = null;
            try {
                mDocument = DocumentHelper.parseText(argData);
                gRoot = mDocument.getRootElement();
                Iterator tIteratorSellBuy = gRoot.elementIterator();
                while(tIteratorSellBuy.hasNext()){
                    Element ElementResult = (Element)tIteratorSellBuy.next();
                    if(ElementResult.getName().equals(FLAG_RESULT)) {
                        Element ElementRowSet = ((Element)ElementResult.elementIterator().next());
                        Iterator ItrStations = ElementRowSet.elementIterator();
                        gListStation = new ArrayList<Station>();
                        ItemParser(ItrStations, gListStation);
                        DbConnector.updateStationList(gListStation);
                    }
                }
            } catch (DocumentException e) {
            }
        }

        public ArrayList<Station> ItemParser(Iterator ItrList, ArrayList<Station> Target) {
            while(ItrList.hasNext()) {
                Element tElement = (Element)ItrList.next();
                Station tStation = new Station();
                tStation.setStationId(Integer.valueOf(tElement.attributeValue("stationID")));
                tStation.setStationName(tElement.attributeValue("stationName"));
                tStation.setSystemId(Integer.valueOf(tElement.attributeValue("solarSystemID")));
                Target.add(tStation);
            }
            System.out.println("Target size = "+Target.size());
            return Target;
        }
    }

    public UpdateStationInfo(){
    }

    @Override
    protected ArrayList<Station> call(){
        EventBaseLookItem mEventBaseLookItem = new EventBaseLookItem();
        mEventBaseLookItem.post();
        return gListStation;
    }
}
