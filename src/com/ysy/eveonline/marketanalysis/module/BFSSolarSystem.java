package com.ysy.eveonline.marketanalysis.module;

import com.ysy.eveonline.marketanalysis.Data.Jump;
import com.ysy.eveonline.marketanalysis.Data.SolarSystem;

import java.util.ArrayList;

public class BFSSolarSystem {
    private ArrayList<SolarSystem> PathData;
    private int JumpCount;

    public BFSSolarSystem(ArrayList<SolarSystem> argSystemList) {
        PathData = new ArrayList<SolarSystem>();
        PathData.addAll(argSystemList);
        JumpCount = 0;
    }

    public Jump BFS(int argStart, int argTarget) {
        Jump result = new Jump();
        ArrayList<Integer> TravelQueue = new ArrayList<Integer>();
        TravelQueue.add(argStart);
        for(int i = 0; i<PathData.size(); i++) {
            if(PathData.get(i).getSystemId() == TravelQueue.get(0)) {
                PathData.get(i).setLevel(0);
                result.setLowestSecurity(PathData.get(i).getSecurity());
                break;
            }
            if(i>=PathData.size()-1) {
                result.setState(Jump.FLAG_SYSTEM_NOT_FOUND);
                return result;
            }
        }
        do {
            for(int i = 0; i<PathData.size(); i++) { // just need to get the node info
                if(PathData.get(i).getSystemId() == TravelQueue.get(0)) { // if found node info
                    JumpCount = PathData.get(i).getLevel();
                    if(TravelQueue.get(0)==argTarget) {
                        result.setJumps(JumpCount);
                        result.setState(Jump.FLAG_SUCCESS);
                        return result;
                    }
                    for(int j = 0; j<PathData.get(i).getNearbyJumps().length; j++) { // list all nearby node
                        for(int k = 0; k<PathData.size(); k++) { // set info change for nearby node
                            if(PathData.get(k).getSystemId()==PathData.get(i).getNearbyJumps()[j]) { // found where the nearby node is
                                if(PathData.get(k).getLevel()==-1) { // never traveled
                                    TravelQueue.add(PathData.get(k).getSystemId()); // add nearby node into queue
                                    PathData.get(k).setLevel(PathData.get(i).getLevel()+1); // set info change
                                    if(PathData.get(k).getSecurity()<PathData.get(i).getSecurity()) {
                                        PathData.get(k).setLowestSecurity(PathData.get(k).getSecurity());
                                        result.setLowestSecurity(PathData.get(k).getSecurity());
                                    } else {
                                        PathData.get(k).setLowestSecurity(PathData.get(i).getSecurity());
                                        result.setLowestSecurity(PathData.get(i).getSecurity());
                                    }
                                }
                                break;
                            }
                            if(k>=PathData.size()-1) {
                                result.setState(Jump.FLAG_SYSTEM_NOT_FOUND);
                                return result;
                            }
                        }
                    }
                    TravelQueue.remove(0);
                    break;
                }
                if(i>=PathData.size()-1) {
                    result.setState(Jump.FLAG_SYSTEM_NOT_FOUND);
                    return result;
                }
            }
        } while(TravelQueue.size()>0); // queue is not empty
        result.setState(Jump.FLAG_UNKNOWN_ERROR);
        return result;
    }
}
