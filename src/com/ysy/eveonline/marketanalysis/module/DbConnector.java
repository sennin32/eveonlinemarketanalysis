package com.ysy.eveonline.marketanalysis.module;

import com.ysy.eveonline.marketanalysis.Data.*;
import com.ysy.eveonline.marketanalysis.Data.SolarSystem;

import java.sql.*;
import java.util.ArrayList;

public class DbConnector {
    private static String SCHEMA_EVE_ONLINE = "ebs_datadump";
    private static String TABLE_ITEM_TYPE = "invtypes";
    private static String TABLE_SYSTEM = "mapsolarsystems";
    private static String TABLE_SYSTEM_JUMP = "mapsolarsystemjumps";
    private static String TABLE_STATION = "mapstationlist";
    private static String TABLE_STATIC_STATION = "stastations";

    private static String COL_COUNT = "COUNT(*)";

    private static String COL_ITEM_ID = "typeID";
    private static String COL_ITEM_NAME = "typeName";
    private static String COL_ITEM_VOLUME = "volume";

    private static String COL_STATION_ID = "stationID";
    private static String COL_STATION_NAME = "stationName";

    private static String COL_SYSTEM_ID = "solarSystemID";
    private static String COL_SYSTEM_NAME = "solarSystemName";
    private static String COL_REGION_ID = "regionID";
    private static String COL_SYSTEM_SECURITY = "security";

    private static String COL_JUMP_START = "fromSolarSystemID";
    private static String COL_JUMP_END = "toSolarSystemID";

    private static String JDBC_CLASS_NAME = "com.mysql.jdbc.Driver";
    private static String DbUrl = "jdbc:mysql://localhost:3306/"+SCHEMA_EVE_ONLINE;
    private static String UserName = "root";
    private static String PassWord = "kamisama77928";

    public static ArrayList<ItemType> getItemList() {
        ArrayList<ItemType> mListItem = new ArrayList<ItemType>();
        try{
            Class.forName(JDBC_CLASS_NAME);
            Connection conDB = DriverManager.getConnection(DbUrl, UserName, PassWord);
            Statement stmt = conDB.createStatement();
            ResultSet rs = stmt.executeQuery("select * from "+SCHEMA_EVE_ONLINE+ "."+ TABLE_ITEM_TYPE);

            while(rs.next()) {
                ItemType tItemType = new ItemType();
                tItemType.setItemId(String.valueOf(rs.getInt(COL_ITEM_ID)));
                tItemType.setItemName(rs.getString(COL_ITEM_NAME));
                tItemType.setVolume(rs.getDouble(COL_ITEM_VOLUME));
                mListItem.add(tItemType);
            }

            rs.close();
            stmt.close();
            conDB.close();
        }catch(SQLException e1){
            e1.printStackTrace();
        }catch(ClassNotFoundException e2){
            e2.printStackTrace();
        }
        return mListItem;
    }

    public static void updateStationList(ArrayList<Station> argList) {
        try{
            Class.forName(JDBC_CLASS_NAME);
            Connection conDB = DriverManager.getConnection(DbUrl, UserName, PassWord);
            String query = " insert into "+ TABLE_STATION+
                    " ("+ COL_STATION_ID+
                    ", "+ COL_STATION_NAME+
                    ", "+ COL_SYSTEM_ID+")"
                    + " values (?, ?, ?)";
            System.out.println("argList size = "+argList.size());
            for(int i=0; i<argList.size(); i++) {
                System.out.println("putStationList : "+i+" : "+
                        "stationID : "+ argList.get(i).getStationId()+
                        ", stationName : "+ argList.get(i).getStationName()+
                        ", solarSystemID : "+ argList.get(i).getSystemId());
                PreparedStatement preparedStmt = conDB.prepareStatement(query);
                preparedStmt.setInt(1, argList.get(i).getStationId());
                preparedStmt.setString(2, argList.get(i).getStationName());
                preparedStmt.setInt(3, argList.get(i).getSystemId());
                preparedStmt.execute();
            }
            conDB.close();
        }catch(SQLException e1){
        }catch(ClassNotFoundException e2){
        }
    }

    public static ArrayList<Station> getStationList() {
        ArrayList<Station> mListStation = new ArrayList<Station>();
        try{
            Class.forName(JDBC_CLASS_NAME);
            Connection conDB = DriverManager.getConnection(DbUrl, UserName, PassWord);
            Statement stmt = conDB.createStatement();

            ResultSet rs = stmt.executeQuery("select * from "+SCHEMA_EVE_ONLINE+ "."+ TABLE_STATIC_STATION);
            while(rs.next()) {
                Station tStation = new Station();
                tStation.setStationId(rs.getInt(COL_STATION_ID));
                tStation.setStationName(rs.getString(COL_STATION_NAME));
                tStation.setSystemId(rs.getInt(COL_SYSTEM_ID));
                mListStation.add(tStation);
            }

            rs = stmt.executeQuery("select * from "+SCHEMA_EVE_ONLINE+ "."+ TABLE_STATION);
            while(rs.next()) {
                Station tStation = new Station();
                tStation.setStationId(rs.getInt(COL_STATION_ID));
                tStation.setStationName(rs.getString(COL_STATION_NAME));
                tStation.setSystemId(rs.getInt(COL_SYSTEM_ID));
                mListStation.add(tStation);
            }

            rs.close();
            stmt.close();
            conDB.close();
        }catch(SQLException e1){
            e1.printStackTrace();
        }catch(ClassNotFoundException e2){
            e2.printStackTrace();
        }
        return mListStation;
    }

    public static ArrayList<SolarSystem> getSystemList() {
        ArrayList<SolarSystem> mListSystem = new ArrayList<SolarSystem>();
        try{
            Class.forName(JDBC_CLASS_NAME);
            Connection conDB = DriverManager.getConnection(DbUrl, UserName, PassWord);
            Statement stmt = conDB.createStatement();
            ResultSet ResultSetSystems = stmt.executeQuery("select * from "+SCHEMA_EVE_ONLINE+ "."+ TABLE_SYSTEM);

            while(ResultSetSystems.next()) {
                SolarSystem tSystem = new SolarSystem();
                tSystem.setSystemId(ResultSetSystems.getInt(COL_SYSTEM_ID));
                tSystem.setSystemName(ResultSetSystems.getString(COL_SYSTEM_NAME));
                tSystem.setRegionId(String.valueOf(ResultSetSystems.getInt(COL_REGION_ID)));
                tSystem.setSecurity(ResultSetSystems.getDouble(COL_SYSTEM_SECURITY));
                tSystem.setNearbyJumps(getSystemJumpList(Integer.valueOf(tSystem.getSystemId())));
                mListSystem.add(tSystem);
            }

            ResultSetSystems.close();
            stmt.close();
            conDB.close();
        }catch(SQLException e1){
            e1.printStackTrace();
        }catch(ClassNotFoundException e2){
            e2.printStackTrace();
        }
        return mListSystem;
    }

    public static SolarSystem getSystem(int argSystemId) {
        SolarSystem mSystem = new SolarSystem();
        try{
            Class.forName(JDBC_CLASS_NAME);
            Connection conDB = DriverManager.getConnection(DbUrl, UserName, PassWord);
            Statement stmt = conDB.createStatement();
            ResultSet ResultSetSystem = stmt.executeQuery("select * from "+ SCHEMA_EVE_ONLINE+ "."+ TABLE_SYSTEM+
            " where "+ COL_SYSTEM_ID+ " = "+argSystemId);

            mSystem.setSystemId(ResultSetSystem.getInt(COL_SYSTEM_ID));
            mSystem.setSystemName(ResultSetSystem.getString(COL_SYSTEM_NAME));
            mSystem.setRegionId(String.valueOf(ResultSetSystem.getInt(COL_REGION_ID)));
            mSystem.setSecurity(ResultSetSystem.getDouble(COL_SYSTEM_SECURITY));
            mSystem.setNearbyJumps(getSystemJumpList(Integer.valueOf(argSystemId)));
            ResultSetSystem.close();

            stmt.close();
            conDB.close();
        }catch(SQLException e1){
        }catch(ClassNotFoundException e2){
        }
        return mSystem;
    }

    public static int[] getSystemJumpList(int argSystemId) {
        int[] ArrayJumps;
        try{
            Class.forName(JDBC_CLASS_NAME);
            Connection conDB = DriverManager.getConnection(DbUrl, UserName, PassWord);
            Statement stmt = conDB.createStatement();

            ResultSet ResultSetCount = stmt.executeQuery("select COUNT(*) from "+SCHEMA_EVE_ONLINE+ "."+ TABLE_SYSTEM_JUMP+
                    " where "+COL_JUMP_START+ " = "+ String.valueOf(argSystemId));
            ResultSetCount.next();
            ArrayJumps = new int[ResultSetCount.getInt(COL_COUNT)];

            ResultSet ResultJumps = stmt.executeQuery("select * from "+SCHEMA_EVE_ONLINE+ "."+ TABLE_SYSTEM_JUMP+
            " where "+COL_JUMP_START+ " = "+ String.valueOf(argSystemId));

            for(int i = 0; i<ArrayJumps.length; i++) {
                ResultJumps.next();
                ArrayJumps[i] = ResultJumps.getInt(COL_JUMP_END);
            }

            ResultJumps.close();
            ResultSetCount.close();
            stmt.close();
            conDB.close();
            return ArrayJumps;
        }catch(SQLException e1){
            e1.printStackTrace();
        }catch(ClassNotFoundException e2){
            e2.printStackTrace();
        }
        return new int[0];
    }
}
