package com.ysy.eveonline.marketanalysis.module;

import com.ysy.eveonline.marketanalysis.Data.EnumSellOrBuy;
import com.ysy.eveonline.marketanalysis.Data.ItemInMarket;

import java.util.ArrayList;

public class QuickSortItemInMarket {
    private ArrayList<ItemInMarket> ItemData;
    EnumSellOrBuy gEnumSellOrBuy;

    public QuickSortItemInMarket(ArrayList<ItemInMarket> argItemInMarketList, EnumSellOrBuy argEnumSellOrBuy) {
        ItemData = argItemInMarketList;
        gEnumSellOrBuy = argEnumSellOrBuy;
    }

    public ArrayList<ItemInMarket> doSort( int left, int right) {
        if(right > left) {
            int newPivotIndex = doPartition(left, right, left);
            doSort(left, newPivotIndex-1);
            doSort(newPivotIndex+1, right);
        }
        return ItemData;
    }

    private int doPartition( int left, int right, int pivotIndex) {
        ItemInMarket pivotItemInMarket = ItemData.get(pivotIndex);
        doSwap(pivotIndex, right);
        int storeIndex = left;
        for(int i=left; i<=right-1; i++) {
            switch (gEnumSellOrBuy) {
                case FLAG_SELL:
                    if(ItemData.get(i).getStartPrice() < pivotItemInMarket.getStartPrice()) {
                        doSwap(storeIndex, i);
                        storeIndex += 1;
                    }
                    break;
                case FLAG_BUY:
                    if(ItemData.get(i).getEndPrice() > pivotItemInMarket.getEndPrice()) {
                        doSwap(storeIndex, i);
                        storeIndex += 1;
                    }
                    break;
            }
        }
        doSwap(right, storeIndex);
        return storeIndex;
    }

    private void doSwap(int argNode1, int argNode2) {
        ItemInMarket tItemInMarket1 = ItemData.get(argNode1), tItemInMarket2 = ItemData.get(argNode2);
        ItemData.remove(argNode1);
        ItemData.add(argNode1, tItemInMarket2);
        ItemData.remove(argNode2);
        ItemData.add(argNode2, tItemInMarket1);
    }
}
