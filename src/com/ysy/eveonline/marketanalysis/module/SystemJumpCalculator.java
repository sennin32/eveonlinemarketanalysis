package com.ysy.eveonline.marketanalysis.module;

import com.ysy.eveonline.marketanalysis.Data.Jump;
import com.ysy.eveonline.marketanalysis.Data.SolarSystem;
import com.ysy.eveonline.marketanalysis.Data.Station;

import java.util.ArrayList;

public class SystemJumpCalculator {
    static SystemJumpCalculator gSystemJumpCalculator;

    private ArrayList<SolarSystem> ListSystemType;
    private ArrayList<Station> ListStation;

    public static SystemJumpCalculator getInstance() {
        if(gSystemJumpCalculator==null) {
            gSystemJumpCalculator = new SystemJumpCalculator();
        }
        return gSystemJumpCalculator;
    }

    public SystemJumpCalculator() {
    }

    public Jump CalculateJumps(int argStart, int argTarget){
        BFSSolarSystem mBFSSolarSystem = new BFSSolarSystem((ArrayList<SolarSystem>)ListSystemType.clone());
        return mBFSSolarSystem.BFS(argStart, argTarget);
    }

    public Jump CalculateStationJumps(int argStart, int argTarget, double argLowestSecurity){
        int systemIdStart=0, systemIdEnd=0;
        for(int i =0; i<ListStation.size(); i++) {
            if(ListStation.get(i).getStationId()==argStart) {
                if(ListStation.get(i).getSecurity()<argLowestSecurity) {
                    Jump tJump = new Jump();
                    tJump.setLowestSecurity(ListStation.get(i).getSecurity());
                    return tJump;
                }
                systemIdStart = ListStation.get(i).getSystemId();
            } else if(ListStation.get(i).getStationId()==argTarget) {
                if(ListStation.get(i).getSecurity()<argLowestSecurity) {
                    Jump tJump = new Jump();
                    tJump.setLowestSecurity(ListStation.get(i).getSecurity());
                    return tJump;
                }
                systemIdEnd = ListStation.get(i).getSystemId();
            }
        }
        ArrayList<SolarSystem> objSolarList = new ArrayList<SolarSystem>();
        for(int i=0; i<ListSystemType.size(); i++) {
            objSolarList.add((SolarSystem)ListSystemType.get(i).clone());
        }
        BFSSolarSystem mBFSSolarSystem = new BFSSolarSystem(objSolarList);
        return mBFSSolarSystem.BFS(systemIdStart, systemIdEnd);
    }

    public void setSystemList(ArrayList<SolarSystem> argListSystem){
        ListSystemType = argListSystem;
    }

    public void setStationList(ArrayList<Station> argListSystem){
        ListStation = argListSystem;
    }
}
